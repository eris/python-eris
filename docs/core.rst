Encoding and Decoding
=====================

The core API is exposed in the ``eris`` module. This allows you to encode an decode content.

The version of the specification implemented is set in `eris.spec_version`:

.. autofunction:: eris.spec_version

.. _store:

Store
-----

Before we can encode or decode content, we need to be able to store and retrieve blocks frome somewhere. This functionality is provided by stores.

.. autosummary::
   :toctree: generated

   eris.Store

Stores can be bindings to in-memory structures, databases or network protocols. See the section :ref:`on storage and transport layers <storage_transport>`.


Basic Stores
^^^^^^^^^^^^

Two basic stores are provided as part of the core API.

.. autosummary::
   :toctree: generated

   eris.NullStore
   eris.DictStore


Encoding Content
----------------

Content can be encoding using an ``eris.Encoder``.

.. autosummary::
   :toctree: generated

   eris.Encoder

Content must be provided as Python `bytes`:

.. code-block:: python

	import eris
	import asyncio

	async def main():
	    encoder = eris.Encoder(eris.null_convergence_secret(), block_size = 1024)

	    await encoder.write(b'Hello world!')
	    
	    read_capability = await encoder.close()

	    print(read_capability)
	
	asyncio.run(main())

Multiple bytes can be added to the encoded content incrementally by calling `encoder.write` multiple times. This allows encoding of content larger than memory

Note that a convergence secret must be provided (see <http://purl.org/eris#name-convergence-secret>). It is safe to use 32 bytes of random byte (e.g. `secrets.token_bytes(32)`). If you are aware of the attacks against convergent encryption and want deterministic identifiers you may use the null convergence secret (32 bytes of zeroes), which is provided in `eris.null_covergence_secret`:

.. autofunction:: eris.null_convergence_secret

Decoding Content
-----------------

Content can be decoded using an ``eris.Decoder``.

.. autosummary::
   :toctree: generated

   eris.Decoder

To decode some previously encoded content:

.. code-block:: python

	import eris
	import asyncio

	async def main():

            # create a store
	    store = eris.DictStore()

	    # encode content
	    encoder = eris.Encoder(eris.null_convergence_secret(), store, block_size = 1024)
	    await encoder.write(b'Hello world!')
	    read_capability = await encoder.close()

	    # decode content
	    decoder = eris.Decoder(store, read_capability)
	    decoded = await decoder.readall()

	    print(decoded)
	
	asyncio.run(main())

Decoders allow efficient random-access to content as specific positions using the `seek` and `read` methods.
