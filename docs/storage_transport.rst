.. _storage_transport:

Storage and Transport
^^^^^^^^^^^^^^^^^^^^^

Blocks of encoded ERIS content can be stored and transported in and over various formats and protocols (see also the section on storage and transport layers in the ERIS specification <http://purl.org/eris#name-storage-and-transport-layer> ).

The :ref:`eris.Store <store>` class provides the generic abstraction over such storage and transport layers. You may use this to create your own bindings to some storage or transport layer.

A few useful storage and transport layers are provided by this library.

CoAP
====

The Constrained Application Protocol (CoAP) as defined in RFC 7252 <https://www.rfc-editor.org/rfc/rfc7252> is a transport protocol designed for constraint environments. It is light-weight, flexible enough and performant enough to use as a reliable transport protocol for ERIS encoded blocks. See also ERIS over CoAP <https://eris.codeberg.page/eer/coap.xml>.

This library provides an :ref:`eris.Store <store>` implementation that can connect to a store via CoAP as well helpers to implement a store that is reachable via CoAP.

Blocks can be accessed and stored using the `eris.coap.Store` class:

.. autosummary::
   :toctree: generated

   eris.coap.Store

Server
------

The function `eris.coap.add_store_resources` can be used to add all store resources to an `aiocoap.resource.Site`:

.. autofunction:: eris.coap.add_store_resources

This can be used to start an ERIS CoAP store listening on TCP port 5683:

.. code-block:: python

	import eris.coap
	import asyncio
	import aiocoap


	async def main():

	    # create a dict store
	    dict_store = eris.DictStore()

	    # setup the CoAP site
	    root = aiocoap.resource.Site()

	    # Add the ERIS store resources, using the dict_store as underlying store
	    eris.coap.add_store_resources(root, dict_store)

	    # start the server
	    await aiocoap.Context.create_server_context(
		root, bind=("127.0.0.1", 5683), transports=["tcpserver"]
	    )

	    # Run forever
	    await asyncio.get_running_loop().create_future()


	asyncio.run(main())

.. note::
   The CoAP server currently does not support block-wise transfer <https://datatracker.ietf.org/doc/html/rfc7959>. This may be required when transporting blocks over UDP as the MTU of UDP may be smaller than the size of single ERIS block. Users are currently advised to use CoAP over TCP <https://www.rfc-editor.org/rfc/rfc8323> (as shown in example above).
