.. python-eris documentation master file, created by
   sphinx-quickstart on Sat Jan 14 09:56:53 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

python-eris
===========

This is a Python implementation of the Encoding for Robust and Immutable Storage (ERIS) <http://purl.org/eris>.

`python-eris` is free software released under the terms of the AGPL-3.0-or-later. The source code is available on Codeberg <https://codeberg.org/eris/python-eris>.

See also the project page <https://eris.codeberg.page/> for more information.

Contents
--------

.. toctree::

   core
   storage_transport


Acknowledgments
---------------

Development of python-eris has been supported by the NLnet Foundation through the NGI Assure Fund <https://nlnet.nl/assure/>.

