import asyncio
import json
import os
import gzip
import random
import unittest

import eris
import eris.coap
import eris.base32 as base32

import aiocoap

random.seed()
portNum = random.randrange(0xc000, 0xffff)

dir = os.path.dirname(__file__)

class CoAPTestVectorTestCase(unittest.TestCase):
    def __init__(self, test_vector_file):
        self.test_vector_file = test_vector_file
        unittest.TestCase.__init__(self)

    def setUp(self):
        filename = os.path.join(dir, "test-vectors", self.test_vector_file)
        with gzip.open(filename) as f:
            self.test_vector = json.load(f)

        # underlying storage of server
        self.dict_store = eris.DictStore()

        # init server attribute
        self.server = None

        # store that connects to the server
        self.store = eris.coap.Store(f'coap+tcp://[::1]:{portNum}/.well-known/eris')

    def id(self):
        return self.test_vector["id"]

    def shortDescription(self):
        return self.test_vector["description"]

    async def start_server(self):
        if not self.server:
            root = aiocoap.resource.Site()
            eris.coap.add_store_resources(root, self.dict_store)
            self.server = await aiocoap.Context.create_server_context(
                root, bind=("::1", portNum), transports=["tcpserver"]
            )

    async def test_encode(self):
        content = base32.decode(self.test_vector["content"])
        convergence_secret = base32.decode(self.test_vector["convergence-secret"])
        block_size = self.test_vector["block-size"]
        expected_urn = self.test_vector["urn"]

        # start the CoAP server
        await self.start_server()

        encoder = eris.Encoder(convergence_secret, self.store, block_size)

        await encoder.write(content)

        read_capability = await encoder.close()

        # encode read_capability as string
        urn = str(read_capability)
        # and check that it is equal to expected URN
        self.assertEqual(expected_urn, urn)

        # decode from
        decoder = eris.Decoder(self.store, expected_urn)
        roundtrip = await decoder.readall()
        self.assertEqual(roundtrip, content)

        await self.store.close()
        await self.server.shutdown()

    def runTest(self):
        asyncio.run(self.test_encode())


positive_test_vectors = [
    "eris-test-vector-positive-00.json.gz",
    "eris-test-vector-positive-01.json.gz",
    "eris-test-vector-positive-02.json.gz",
    "eris-test-vector-positive-03.json.gz",
    "eris-test-vector-positive-04.json.gz",
    "eris-test-vector-positive-05.json.gz",
    "eris-test-vector-positive-06.json.gz",
    "eris-test-vector-positive-07.json.gz",
    "eris-test-vector-positive-08.json.gz",
    "eris-test-vector-positive-09.json.gz",
    "eris-test-vector-positive-10.json.gz",
    "eris-test-vector-positive-11.json.gz",
    "eris-test-vector-positive-12.json.gz",
]


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    for test_vector in positive_test_vectors:
        suite.addTest(CoAPTestVectorTestCase(test_vector_file=test_vector))
    return suite


if __name__ == "__main__":
    unittest.main()
