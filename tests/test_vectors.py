import os
import gzip
import json
import unittest
import asyncio
import eris

from eris import base32

dir = os.path.dirname(__file__)


class PositiveTestVectorTestCase(unittest.TestCase, eris.Store):
    def __init__(self, test_vector_file):
        self.test_vector_file = test_vector_file
        unittest.TestCase.__init__(self)

    def setUp(self):
        filename = os.path.join(dir, "test-vectors", self.test_vector_file)
        with gzip.open(filename) as f:
            self.test_vector = json.load(f)
        self.dict_store = eris.DictStore()

    def id(self):
        return self.test_vector["id"]

    def shortDescription(self):
        return self.test_vector["description"]

    async def test_encode(self):
        content = base32.decode(self.test_vector["content"])
        convergence_secret = base32.decode(self.test_vector["convergence-secret"])
        block_size = self.test_vector["block-size"]
        expected_urn = self.test_vector["urn"]

        encoder = eris.Encoder(
            convergence_secret,
            self.dict_store,
            block_size,
        )

        await encoder.write(content)

        read_capability = await encoder.close()

        # encode read_capability as string
        urn = str(read_capability)
        # and check that it is equal to expected URN
        self.assertEqual(expected_urn, urn)

        # decode from test vector
        decoder = eris.Decoder(self, expected_urn)
        roundtrip = await decoder.readall()
        self.assertEqual(roundtrip, content)

        # decode from dict
        decoder = eris.Decoder(self.dict_store, expected_urn)
        roundtrip = await decoder.readall()
        self.assertEqual(roundtrip, content)

        await self.dict_store.close()

    # implements a store that access blocks in test vector
    async def get(self, ref, block_size=False):
        ref_b32 = base32.encode(ref)
        block_b32 = self.test_vector["blocks"][ref_b32]
        if block_b32:
            return base32.decode(block_b32)
        else:
            return False

    def runTest(self):
        asyncio.run(self.test_encode())


positive_test_vectors = [
    "eris-test-vector-positive-00.json.gz",
    "eris-test-vector-positive-01.json.gz",
    "eris-test-vector-positive-02.json.gz",
    "eris-test-vector-positive-03.json.gz",
    "eris-test-vector-positive-04.json.gz",
    "eris-test-vector-positive-05.json.gz",
    "eris-test-vector-positive-06.json.gz",
    "eris-test-vector-positive-07.json.gz",
    "eris-test-vector-positive-08.json.gz",
    "eris-test-vector-positive-09.json.gz",
    "eris-test-vector-positive-10.json.gz",
    "eris-test-vector-positive-11.json.gz",
    "eris-test-vector-positive-12.json.gz",
]


class NegativeTestVectorTestCase(unittest.TestCase, eris.Store):
    def __init__(self, test_vector_file):
        self.test_vector_file = test_vector_file
        unittest.TestCase.__init__(self)

    def setUp(self):
        filename = os.path.join(dir, "test-vectors", self.test_vector_file)
        with gzip.open(filename) as f:
            self.test_vector = json.load(f)
        self.dict_store = eris.DictStore()

    def id(self):
        return self.test_vector["id"]

    def shortDescription(self):
        return self.test_vector["description"]

    async def test_decode(self):
        urn = self.test_vector["urn"]

        # decode from test vector
        decoder = eris.Decoder(self, urn)

        with self.assertRaises(eris.decoder.DecodeError):
            await decoder.readall()

    # implements a store that access blocks in test vector
    async def get(self, ref, block_size=False):
        ref_b32 = base32.encode(ref)
        if ref_b32 in self.test_vector["blocks"]:
            return base32.decode(self.test_vector["blocks"][ref_b32])
        else:
            None

    def runTest(self):
        asyncio.run(self.test_decode())


negative_test_vectors = [
    "eris-test-vector-negative-13.json.gz",
    "eris-test-vector-negative-14.json.gz",
    "eris-test-vector-negative-15.json.gz",
    "eris-test-vector-negative-16.json.gz",
    "eris-test-vector-negative-17.json.gz",
    "eris-test-vector-negative-18.json.gz",
    "eris-test-vector-negative-19.json.gz",
    "eris-test-vector-negative-20.json.gz",
    "eris-test-vector-negative-21.json.gz",
    "eris-test-vector-negative-22.json.gz",
    "eris-test-vector-negative-23.json.gz",
    "eris-test-vector-negative-24.json.gz",
]


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    for test_vector in positive_test_vectors:
        suite.addTest(PositiveTestVectorTestCase(test_vector_file=test_vector))

    for test_vector in negative_test_vectors:
        suite.addTest(NegativeTestVectorTestCase(test_vector_file=test_vector))

    return suite


if __name__ == "__main__":
    unittest.main()
