import eris
import eris.coap
import asyncio


async def main():

    # create a store
    store = eris.coap.Store("coap+tcp://127.0.0.1/.well-known/eris")

    # encode content
    encoder = eris.Encoder(eris.null_convergence_secret(), store, block_size=1024)
    await encoder.write(b"Hello world!")
    read_capability = await encoder.close()

    # decode content
    decoder = eris.Decoder(store, read_capability)
    decoded = await decoder.readall()

    print(decoded)


asyncio.run(main())
