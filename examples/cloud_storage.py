# SPDX-FileCopyrightText: 2023 pukkamustard <pukkamustard@posteo.net>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import eris
import asyncio
import boto3


class CloudObjectStore(eris.Store):
    """An ERIS store backed by a cloud object storage"""

    def create_bucket(self, region):
        """Attempt to create the bucket in the specified region."""
        try:
            response = self.client.create_bucket(
                Bucket=self.bucket_name,
                ACL="public-read",
                CreateBucketConfiguration={"LocationConstraint": region},
            )
            print(response)
        except:
            pass

    def __init__(self, **kwargs):
        """Returns a store connected to a cloud object storage.

        Parameters:

        endpoint_url: The HTTP/HTTPS endpoint of the cloud object storage.
        access_key_id: The Access Key ID
        secret_access_key: The secret access key
        """
        self.client = boto3.client(
            service_name="s3",
            endpoint_url=kwargs["endpoint_url"],
            aws_access_key_id=kwargs["access_key_id"],
            aws_secret_access_key=kwargs["secret_access_key"],
        )
        self.bucket_name = kwargs["bucket"]

    async def get(self, ref, block_size=False):
        key = eris.base32.encode(ref)
        response = self.client.get_object(Bucket=self.bucket_name, Key=key)
        return response["Body"].read()

    async def put(self, ref, block):
        key = eris.base32.encode(ref)
        self.client.put_object(
            Bucket=self.bucket_name, ACL="public-read", Body=block, Key=key
        )

    async def close(self):
        pass


async def main():
    store = CloudObjectStore(
        endpoint_url="https://s3.eu-central-1.wasabisys.com",
        access_key_id="XXXX",
        secret_access_key="XXXX",
        bucket="eris-blocks",
    )

    store.create_bucket("eu-central-1")

    # encode content
    encoder = eris.Encoder(eris.null_convergence_secret(), store, block_size=32768)

    with open("cloud_storage.py", encoding="utf-8") as file:
        for line in file:
            await encoder.write(line.encode())

    read_capability = await encoder.close()

    # decode content
    decoder = eris.Decoder(store, read_capability)
    decoded_bytes = await decoder.readall()

    # decode UTF-8
    decoded = decoded_bytes.decode()

    print(decoded)


asyncio.run(main())
