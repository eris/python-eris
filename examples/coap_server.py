import eris.coap
import asyncio
import aiocoap


async def main():

    # create a dict store
    dict_store = eris.DictStore()

    # setup the CoAP site
    root = aiocoap.resource.Site()

    # Add the ERIS store resources, using the dict_store as underlying store
    eris.coap.add_store_resources(root, dict_store)

    # start the server
    await aiocoap.Context.create_server_context(
        root, bind=("127.0.0.1", 5683), transports=["tcpserver"]
    )

    # Run forever
    await asyncio.get_running_loop().create_future()


    

asyncio.run(main())
