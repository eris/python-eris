import eris
import asyncio


async def main():

    # create a store
    store = eris.DictStore()

    # encode content
    encoder = eris.Encoder(eris.null_convergence_secret(), store, block_size=1024)

    with open('encode_file.py', encoding="utf-8") as file:
        for line in file:
            await encoder.write(line.encode())

    read_capability = await encoder.close()

    # decode content
    decoder = eris.Decoder(store, read_capability)
    decoded_bytes = await decoder.readall()

    # decode UTF-8
    decoded = decoded_bytes.decode()

    print(decoded)


asyncio.run(main())
