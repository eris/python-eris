(use-modules
  (guix packages)
  ((guix licenses) #:prefix license:)
  (guix download)
  (guix build-system python)
  (gnu packages)
  (gnu packages check)
  (gnu packages python)
  (gnu packages python-build)
  (gnu packages python-check)
  (gnu packages python-crypto)
  (gnu packages python-web)
  (gnu packages python-xyz)
  (gnu packages sphinx)
  (gnu packages license))

(define-public python-linkheader
  (package
    (name "python-linkheader")
    (version "0.4.3")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "LinkHeader" version))
              (sha256
               (base32
                "1hsh3gr8d5mc91c396xj33bv6zbfi0f7xnvi0m9vryx31dfc7fvz"))))
    (build-system python-build-system)
    (home-page "http://bitbucket.org/asplake/link_header")
    (synopsis
     "Parse and format link headers according to RFC 5988 \"Web Linking\"")
    (description
     "Parse and format link headers according to RFC 5988 \"Web Linking\"")
    (license license:bsd-2)))

(define-public python-aiocoap
  (package
    (name "python-aiocoap")
    (version "0.4.5")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "aiocoap" version))
              (sha256
               (base32
                "1qx1dfyxqdwiqfay7mgmmcfz6ifqizcivk7x0k7qlp0q49w94v4h"))))
    (build-system python-build-system)
    (arguments `(#:tests? #f))
    (propagated-inputs
     (list python-pygments
	   python-linkheader
	   python-cbor2
	   python-cryptography
	   python-filelock))
    (home-page "https://github.com/chrysn/aiocoap")
    (synopsis "Python CoAP library")
    (description "Python CoAP library")
    (license license:expat)))

(package
  (name "pyton-eris")
  (version "0.1.0")
  (source #f)
  (build-system python-build-system)
  (propagated-inputs
   (list python-pycryptodome
	 python-aiocoap))
  (native-inputs
   (list python-setuptools
	 python-pypa-build
	 python-twine
	 python-pylint
	 python-pyflakes
	 python-black
	 ;;python-sphinx
	 reuse

	 ;; cloud storage example
	 python-boto3))
  (synopsis "Python implementation of the Encoding for Robust Immutable Storage (ERIS)")
  (description #f)
  (home-page "https://inqlab.net/git/python-eris.git/")
  (license license:lgpl3+))
