let
  pkgs = import <nixpkgs> { };
in
(pkgs.python3.withPackages (
  pkgs:
  builtins.attrValues {
    inherit (pkgs)
      aiocoap
      pycryptodome
      ;
  }
)).env
